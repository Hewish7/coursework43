**Group Number:** Coursework 43   
**Group Members:**   
1. Gaurav Kumar   
2. Hewishsingh Ramchurreetoo   
3. Adiba Firoz Syed   
4. Prajwal Thakre   
5. Shantam Wadhwa   

**PROJECT TITLE:** Do home teams perform better than away teams in international football matches?   
**Module Name:**Team Research and Development PROJECT   
**Module Code:** 7COM1079-0901-2019   
